var gulp                  = require('gulp'),
    concat                = require('gulp-concat'),
    angularTemplateCache  = require('gulp-angular-templatecache'),
    mainBowerFiles        = require('main-bower-files'),
    ngAnnotate            = require('gulp-ng-annotate'),
    karma                 = require('karma').Server,
    babel                 = require('gulp-babel'),
    filter                = require('gulp-filter'),
    order                 = require('gulp-order'),
    debug                 = require('gulp-debug'),
    runSequence           = require('run-sequence'),
    jshint                = require('gulp-jshint'),
    rimraf                = require('gulp-rimraf'),
    sass                  = require('gulp-sass'),
    image                 = require('gulp-imagemin'),
    ngConstant            = require('gulp-ng-constant'),
    through2              = require('through2');
  // Modules for webserver and livereload
  var express = require('express'),
      refresh = require('gulp-livereload'),
      livereload = require('connect-livereload'),
      livereloadport = 35729,
      serverport = 5000;

  // Set up an express server
  var server = express();
  // Add live reload
  server.use(livereload({port: livereloadport}));
  // Use 'dist' folder as rootfolder
  var folderToServe = __dirname + '/../../dist';
  console.log('Folder to serve: ' + folderToServe);
  server.use(express.static(__dirname + '/../../dist'));
  // HTML5 pushstate .. this redirects everything back to index.html
  server.all('/*', function(req, res) {
    res.sendFile('index.html', { root: 'dist' });
  });

var publicDir             = 'dist',
    publicStylesDir       = 'dist/styles',
    publicScriptsDir      = 'dist/scripts',
    publicImagesDir       = 'dist/images',
    publicFontsDir        = 'dist/fonts',
    devLocalesFiles       = 'app/locales/*json',
    devTemplateFiles      = 'app/templates/**',
    devMainStyleFile      = 'app/styles/main.sass',
    devStyleFiles         = ['app/styles/*.scss', 'app/styles/*.sass', 'app/styles/**/*.scss', 'app/styles/**/*.sass'],
    devScriptFiles        = ['app/scripts/*.js', 'app/scripts/**/*.js'],
    devImageFiles         = 'app/images/*',
    publicLocalesDir      = 'dist/locales'


// Clean task
gulp.task('clean', function() {
  return gulp.src(['dist','coverage'], { read: false }) // much faster without reading
  .pipe(rimraf({force: true}));
});

// Template cache task
gulp.task('template_cache', function() {
  return gulp.src(devTemplateFiles)
    .pipe(angularTemplateCache())
    .pipe(gulp.dest(publicScriptsDir));
});

// Bower [dev] dependencies
gulp.task('bower_js_dev', function () {
  var jsFilter = filter(['**/*.js']);

  // grab vendor js files from bower_components
  return gulp.src(mainBowerFiles())
    .pipe(jsFilter)
    .pipe(debug({files: mainBowerFiles()}))
    .pipe(concat('vendor.js'))
    .pipe(order([
      'angular.js',
      '*'
    ]))
    .pipe(gulp.dest(publicScriptsDir));
});

gulp.task('bower_css_dev', function () {
  var cssFilter = filter(['**/*.css']);
  var files = mainBowerFiles();
  files.push('bower_components/font-awesome/css/*.min.css');

  // grab vendor css files from bower_components
  return gulp.src(files)
    .pipe(cssFilter)
    .pipe(debug({files: files}))
    .pipe(concat('vendor.css'))
    .pipe(gulp.dest(publicStylesDir));
});

gulp.task('bower_fonts_dev', function () {
  var fontFilter = filter(['**/*.eot', '**/*.woff', '**/*.woff2', '*.otf', '**/*.svg', '**/*.ttf']);
  var files = mainBowerFiles();
  files.push('bower_components/font-awesome/fonts/*');
  // grab vendor font files from bower_components
  return gulp.src(files)
    .pipe(fontFilter)
    .pipe(debug({files: files}))
    .pipe(gulp.dest(publicFontsDir));
});

gulp.task('config', function () {
  gulp.src('app/config.json')
    .pipe(ngConstant({
      name: 'portfolioApp.config',
      deps: [],
      constants: {
        API_URL: process.env.API_URL || 'http://api.alexstoicuta.ro'
      },
    }))
    .pipe(through2.obj(function(chunk, enc, callback){
      var str = chunk.contents.toString();
      str = str.replace(/"/g, '\'');

      chunk.contents = new Buffer(str);

      callback(null, chunk);
    }))
    .pipe(gulp.dest('app/scripts/config'));
});

// Move index task
gulp.task('move_index', function() {
  return gulp.src('app/index.html')
    .pipe(gulp.dest(publicDir));
});

gulp.task('move_locales', function() {
  return gulp.src(devLocalesFiles)
    .pipe(gulp.dest(publicLocalesDir));
});

// Styles devel task
gulp.task('styles_dev', function() {
  return gulp.src(devMainStyleFile)
    // The onerror handler prevents Gulp from crashing when you make a mistake in SASS
    .pipe(sass({onError: function(e) { console.log(e); } }))
    .pipe(concat('main.css'))
    .pipe(gulp.dest(publicStylesDir));
});

// JS check task
gulp.task('js_check', function() {
  return gulp.src(devScriptFiles)
    .pipe(jshint())
    .pipe(jshint.reporter('default'));
});

// Scripts [devel] task
gulp.task('scripts_dev', function() {
  return gulp.src(devScriptFiles)
    .pipe(babel({
            presets: ['es2015']
        }))
    .pipe(concat('main.js'))
    .pipe(ngAnnotate({
      "disallowUnknownReferences": true
    }))
    .pipe(gulp.dest(publicScriptsDir));
});

// Images task
gulp.task('images', function() {
  return gulp.src(devImageFiles)
    // .pipe(image())
    .pipe(gulp.dest(publicImagesDir));
});

gulp.task('test', function(done) {
  karma.start({
    configFile: __dirname + '/../../karma.conf.js',
    singleRun: true
  }, function(karmaExitStatus) {
      if (karmaExitStatus) {
        process.exit(1);
      }
    });
});

gulp.task('watch', ['js_check'], function() {
  // Start webserver
  server.listen(serverport);
  // Start live reload
  refresh.listen(livereloadport);

  gulp.watch(devTemplateFiles, [
    'template_cache'
  ]);

  gulp.watch('app/index.html', function() { runSequence('move_index');
  });

  gulp.watch(devLocalesFiles, function() { runSequence('move_locales');
  });

  // Watch our scripts, and when they change run js_check
  gulp.watch(devScriptFiles,[
    'js_check',
    'scripts_dev'
  ]);

  // Watch the SASS files
  gulp.watch(devStyleFiles, [
    'styles_dev'
  ]);

  gulp.watch('dist/**/*').on('change', refresh.changed);
});

gulp.task('live', function() {
  // Start webserver
  server.listen(serverport);
  // Start live reload
  refresh.listen(livereloadport);
});

// Production task ($ gulp)
gulp.task('default', ['clean'], function() {
  return  runSequence('config', 'template_cache', 'move_index', 'move_locales', ['bower_js_dev', 'bower_css_dev', 'bower_fonts_dev'],  ['styles_dev', 'scripts_dev', 'images'], 'watch');
});