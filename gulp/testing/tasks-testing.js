var gulp                  = require('gulp'),
    concat                = require('gulp-concat'),
    angularTemplateCache  = require('gulp-angular-templatecache'),
    mainBowerFiles        = require('main-bower-files'),
    ngAnnotate            = require('gulp-ng-annotate'),
    babel                 = require('gulp-babel'),
    filter                = require('gulp-filter'),
    order                 = require('gulp-order'),
    debug                 = require('gulp-debug'),
    runSequence           = require('run-sequence'),
    publicScriptsDir      = 'dist/scripts',
    devTemplateFiles      = 'app/templates/**';

// Template cache task
gulp.task('template_cache', function() {
  return gulp.src(devTemplateFiles)
    .pipe(angularTemplateCache())
    .pipe(gulp.dest(publicScriptsDir));
});

// Bower [dev] dependencies
gulp.task('bower_js_dev', function () {
  var jsFilter = filter(['**/*.js']);

  // grab vendor js files from bower_components
  return gulp.src(mainBowerFiles())
    .pipe(jsFilter)
    .pipe(debug({files: mainBowerFiles()}))
    .pipe(concat('vendor.js'))
    .pipe(order([
      'angular.js',
      '*'
    ]))
    .pipe(gulp.dest(publicScriptsDir));
});