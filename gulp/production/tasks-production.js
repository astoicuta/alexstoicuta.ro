var gulp                  = require('gulp'),
    concat                = require('gulp-concat'),
    angularTemplateCache  = require('gulp-angular-templatecache'),
    mainBowerFiles        = require('main-bower-files'),
    ngAnnotate            = require('gulp-ng-annotate'),
    karma                 = require('karma').Server,
    babel                 = require('gulp-babel'),
    filter                = require('gulp-filter'),
    runSequence           = require('run-sequence'),
    debug                 = require('gulp-debug'),
    rimraf                = require('gulp-rimraf'),
    sass                  = require('gulp-sass'),
    autoprefixer          = require('gulp-autoprefixer'),
    image                 = require('gulp-imagemin'),
    minifyCss             = require('gulp-minify-css'),
    uglify                = require('gulp-uglify'),
    ngConstant            = require('gulp-ng-constant'),
    through2              = require('through2'),
    gutil                 = require( 'gulp-util' ),
    ftp                   = require( 'vinyl-ftp' ),
    publicDir             = 'dist',
    publicStylesDir       = 'dist/styles',
    publicScriptsDir      = 'dist/scripts',
    publicImagesDir       = 'dist/images',
    publicFontsDir        = 'dist/fonts',
    devLocalesFiles       = 'app/locales/*json',
    devTemplateFiles      = 'app/templates/**',
    devMainStyleFile      = 'app/styles/main.scss',
    devStyleFiles         = ['app/styles/*.scss', 'app/styles/**/*.scss'],
    devScriptFiles        = ['app/scripts/*.js', 'app/scripts/**/*.js', '!app/scripts/controllers/Styleguide/*.js'],
    styleguideScriptFiles = 'app/scripts/controllers/Styleguide/*.js',
    devImageFiles         = 'app/images/*',
    publicLocalesDir      = 'dist/locales'


// Clean task
gulp.task('clean', function() {
  return gulp.src(['dist','coverage'], { read: false }) // much faster without reading
  .pipe(rimraf({force: true}));
});

// Template cache task
gulp.task('template_cache', function() {
  return gulp.src(devTemplateFiles)
    .pipe(angularTemplateCache())
    .pipe(gulp.dest(publicScriptsDir));
});

gulp.task('bower_js_prod', function () {
  var jsFilter = filter(['**/*.js']);

  // grab vendor js files from bower_components
  return gulp.src(mainBowerFiles())
    .pipe(jsFilter)
    .pipe(concat('vendor.js'))
    .pipe(uglify())
    .pipe(gulp.dest(publicScriptsDir));
});

gulp.task('bower_css_prod', function () {
  var cssFilter = filter(['**/*.css']);

  // grab vendor css files from bower_components
  return gulp.src(mainBowerFiles())
    .pipe(cssFilter)
    .pipe(concat('vendor.css'))
    .pipe(minifyCss())
    .pipe(gulp.dest(publicStylesDir));
});

gulp.task('bower_fonts_prod', function () {
  var fontFilter = filter(['**/*.eot', '**/*.woff', '**/*.woff2', '**/*.svg', '**/*.ttf']);

  // grab vendor font files from bower_components
  return gulp.src(mainBowerFiles())
    .pipe(fontFilter)
    .pipe(gulp.dest(publicFontsDir));
});

// Move index task
gulp.task('move_index', function() {
  return gulp.src('app/index.html')
    .pipe(gulp.dest(publicDir));
});

gulp.task('move_locales', function() {
  return gulp.src(devLocalesFiles)
    .pipe(gulp.dest(publicLocalesDir));
});

// Styles production task
gulp.task('styles_prod', function() {
  return gulp.src(devMainStyleFile)
    // The onerror handler prevents Gulp from crashing when you make a mistake in SASS
    .pipe(sass({onError: function(e) { console.log(e); } }))
    .pipe(autoprefixer('last 3 versions', 'ie 8'))
    .pipe(concat('main.css'))
    .pipe(minifyCss())
    .pipe(gulp.dest(publicStylesDir));
});

// Scripts production task
gulp.task('scripts_prod', function() {
  return gulp.src(devScriptFiles)
    .pipe(debug({files: devScriptFiles}))
    .pipe(babel({
            presets: ['es2015']
        }))
    .pipe(concat('main.js'))
    .pipe(ngAnnotate())
    .pipe(uglify())
    .pipe(gulp.dest(publicScriptsDir));
});


// Images task
gulp.task('images', function() {
  return gulp.src(devImageFiles)
    // .pipe(image())
    .pipe(gulp.dest(publicImagesDir));
});

gulp.task('test', function(done) {
  karma.start({
    configFile: __dirname + '/../../karma.conf.js',
    singleRun: true
  }, function(karmaExitStatus) {
      if (karmaExitStatus) {
        process.exit(1);
      }
    });
});

gulp.task('config', function () {
  gulp.src('app/config.json')
    .pipe(ngConstant({
      name: 'portfolioApp.config',
      deps: [],
      constants: {
        API_URL: process.env.API_URL || 'http://api.alexstoicuta.ro'
      },
    }))
    .pipe( through2.obj(function(chunk, enc, callback){
      var str = chunk.contents.toString();
      str = str.replace(/"/g, '\'');

      chunk.contents = new Buffer(str);

      callback(null, chunk);
    }))
    .pipe(gulp.dest('app/scripts/config'));
});


// Production task ($ gulp)
gulp.task('default', ['clean'], function() {
  return runSequence('config','template_cache', 'move_index', 'move_locales', ['bower_js_prod', 'bower_css_prod', 'bower_fonts_prod'],  ['styles_prod', 'scripts_prod', 'images']);
});

gulp.task( 'deploy', function () {

    var conn = ftp.create( {
        host:     process.env.HOST || 'alexstoicuta.ro',
        user:     process.env.USERNAME,
        password: process.env.PASSWORD,
        parallel: 1,
        log:      gutil.log
    } );

    // using base = '.' will transfer everything to /public_html correctly
    // turn off buffering in gulp.src for best performance

    return gulp.src( 'dist/**', { base: 'dist/', buffer: false } )
        .pipe( conn.newer( '/public_html' ) ) // only upload newer files
        .pipe( conn.dest( '/public_html' ) );

} );