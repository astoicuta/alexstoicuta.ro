angular.module('portfolioApp')
     .filter('dateFilter', dateFilter);

function dateFilter () {
  return function(date, format){
    var formattedDate = moment(new Date(date));
    if (formattedDate.isValid()) {
      return formattedDate.format(format);
    }
    else {
      return date;
    }
  };

}

angular.module('portfolioApp')
     .filter('htmlToPlainText', htmlToPlainText);

function htmlToPlainText () {
  return function(text){
    return  text ? String(text).replace(/<[^>]+>/gm, '') : '';
  };

}