/* jshint esversion: 6 */
class postService {
  
  constructor($http, API_URL) {
  	this.httpService	= $http;
  	this.API_URL 		= API_URL;
    console.log('post service');
  }

  getAllPosts() {
  	return this.httpService.get(this.API_URL + '/wp-json/posts');
  }

  getPostBySlug(slug) {
  	let url = this.API_URL + '/wp-json/posts?filter[name]=' + slug + '&_embed'
  	return this.httpService.get(url);
  }

  getPageBySlug(slug) {
  	console.log('getting page by slug', slug);
  	let url = this.API_URL + '/wp-json/pages?filter[name]=' + slug + '&_embed'
  	return this.httpService.get(url);
  }

}

angular.module('portfolioApp').service('postService', postService);