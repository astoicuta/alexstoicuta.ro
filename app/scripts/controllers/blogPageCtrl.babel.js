/* jshint esversion: 6 */
class BlogPageController {
  constructor(postService, $state, $rootScope) {
  	this.stateService 		= $state;
    this.postService        = postService;
    this.rootScope 			= $rootScope;
    console.log(this.stateService);
    this.getCurrentPost();
  }

  getCurrentPost() {
  	this.postService.getPageBySlug(this.stateService.current.url).then(response => {
  		console.log(response);
  		this.post = response.data[0];
  		this.rootScope.requestInProgress = false;
  	}, reason => {
  		this.stateService.go('error');
  	});
  }
}

angular.module('portfolioApp').controller('blogPageCtrl', BlogPageController);