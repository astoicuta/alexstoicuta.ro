/* jshint esversion: 6 */
class BlogIndexController {
  constructor(postService, $rootScope) {
    this.postService   = postService;
    this.rootScope     = $rootScope; 
    this.getPosts();
  }

  getPosts() {
  	this.postService.getAllPosts().then(response => {
  		this.posts = response.data;
  		this.rootScope.requestInProgress = false;
  	}, reason => {
  		this.stateService.go('error');
  	});
  }
}

angular.module('portfolioApp').controller('blogIndexCtrl', BlogIndexController);