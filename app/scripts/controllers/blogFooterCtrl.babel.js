/* jshint esversion: 6 */
class blogFooterController {
  constructor(socialMediaLinks) {
  	this.socialMediaLinks = socialMediaLinks;
  }
}

angular.module('portfolioApp').controller('blogFooterCtrl', blogFooterController);