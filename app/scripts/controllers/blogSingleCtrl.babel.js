/* jshint esversion: 6 */
class BlogSingleController {
  constructor(postService, $state, $rootScope, ngMeta, $filter) {
  	this.stateService 		= $state;
    this.postService      = postService;
    this.rootScope 			  = $rootScope;
    this.ngMeta           = ngMeta;
    this.filter           = $filter;
    this.getCurrentPost();
  }

  getCurrentPost() {
  	this.postService.getPostBySlug(this.stateService.params.postSlug).then(response => {
  		this.post = response.data[0];
  		this.rootScope.requestInProgress = false;
      this.setMetaData();
  	}, reason => {
  		this.stateService.go('error');
  	});
  }

  setMetaData() {
    this.ngMeta.setTitle(this.post.title);
    this.ngMeta.setTag('description', this.filter('htmlToPlainText')(this.post.excerpt));
  }

}

angular.module('portfolioApp').controller('blogSingleCtrl', BlogSingleController);