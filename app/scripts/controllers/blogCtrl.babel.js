/* jshint esversion: 6 */
class BlogController {
  constructor($rootScope, $state) {
    $rootScope.requestInProgress 	= true;
	$rootScope.$on('$stateChangeStart', 
	function(event, toState, toParams, fromState, fromParams){ 
	    $rootScope.requestInProgress 	= true;
	})
  }
}

angular.module('portfolioApp').controller('blogCtrl', BlogController);