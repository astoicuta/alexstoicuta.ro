/* jshint esversion: 6 */
class IntroController {
  constructor() {
    console.log('Controller goes here');
  }
}

angular.module('portfolioApp').controller('introCtrl', IntroController);