angular.module('portfolioApp.config', [])

.constant('socialMediaLinks', {
	'facebookLink': 'http://www.facebook.com/alex.stoicuta',
	'linkedInLink': 'http://ro.linkedin.com/in/alexstoicuta',
	'bitbucketLink': 'https://bitbucket.org/astoicuta/'
})

.constant('API_URL', 'http://api.alexstoicuta.ro')

;