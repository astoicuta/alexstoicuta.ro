'use strict';
var app;

app = angular.module('portfolioApp', ['ng', 'ngSanitize', 'ui.router', 'templates', 'ngAnimate', 'portfolioApp.config', 'pascalprecht.translate', 'ngAnimate', 'ngMeta']);

app.config(['$stateProvider', '$urlRouterProvider', '$locationProvider', 'ngMetaProvider',
  function($stateProvider, $urlRouterProvider, $locationProvider, ngMetaProvider){
    $urlRouterProvider.otherwise('/');
    $stateProvider.state('index', {
      url: '/',
      templateUrl: '_main.html',
      controller: 'introCtrl',
    }).state('error', {
      url: '/error',
      templateUrl: 'error.html',
    }).state('blog', {
      abstract: true,
      templateUrl: 'blog/_layout.html',
      controller: 'blogCtrl',
      controllerAs: 'ctrl'
    }).state('blog.layout', {
      views: {
        'header': {
          templateUrl: 'blog/_header.html'
        },
        'navBar': {
          templateUrl: 'blog/_navBar.html'
        },
        'footer': {
          controller: 'blogFooterCtrl',
          controllerAs: 'ctrl',
          templateUrl: 'blog/_footer.html'
        },
        'content': {
          templateUrl: 'blog/_content.html'
        },
      }
    }).state('blog.layout.about', {
      url: '/about-me',
      templateUrl: 'blog/_page.html',
      controller: 'blogPageCtrl',
      controllerAs: 'ctrl'
    }).state('blog.layout.contact', {
      url: '/contact',
      templateUrl: 'blog/_page.html',
      controller: 'blogPageCtrl',
      controllerAs: 'ctrl'
    }).state('blog.layout.index', {
      url: '/blog',
      templateUrl: 'blog/_index.html',
      controller: 'blogIndexCtrl',
      controllerAs: 'ctrl',
      meta: {
        'title': 'Alex Stoicuta - Blog'
      }
    }).state('blog.layout.blog-post', {
      url: '/blog/:postSlug',
      params: {
        postSlug: 'some-slug'
      },
      templateUrl: 'blog/_single.html',
      controller: 'blogSingleCtrl',
      controllerAs: 'ctrl',
      meta: {
        'type': 'article'
      }
    });
    $locationProvider.html5Mode(true);

    // Setup default values
    ngMetaProvider.setDefaultTag('title', 'Alex Stoicuta - Software developer');
    ngMetaProvider.setDefaultTag('description', 'Everything JS and other unrelated content');
    ngMetaProvider.setDefaultTag('type', 'website');
  }
]);

app.run(['ngMeta', function(ngMeta) { 
  ngMeta.init();
}]);

app.config(['$translateProvider', 
  function translateConfig($translateProvider) {
    $translateProvider.useUrlLoader('locales/en.json');
    $translateProvider.preferredLanguage('en_GB');
    $translateProvider.useSanitizeValueStrategy('sanitize');
  }
]);


angular.module("templates", []);

