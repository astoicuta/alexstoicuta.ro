'use strict';
var env         = process.env.ENV || 'production';
var requireDir  = require('require-dir');
if(env === 'testing') {
  requireDir('./gulp/testing');
} else if(env === 'development') {
  requireDir('./gulp/development');
} else {
  requireDir('./gulp/production');
}